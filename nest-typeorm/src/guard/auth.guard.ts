import { CanActivate, ExecutionContext, Injectable, Logger, HttpException, HttpStatus } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import * as url from 'url';
import { jwt as jwtUtil } from '../utils/jwt';
import { RedisUtilsService } from '../modules/redis-utils/redis-utils.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private readonly redisUtilsService: RedisUtilsService
  ) {
  }

  async canActivate(
    context: ExecutionContext,
  ): Promise<boolean> {
    console.log('守卫');
    const request = context.switchToHttp().getRequest();

    // token可能是在请求头,请求数据,url地址中,不管是在哪里传递过来的都接收处理
    const token =
      context.switchToRpc().getData().headers.token ||
      context.switchToHttp().getRequest().body.token ||
      this.getUrlQuery(request.url, 'token');

    Logger.log(`当前的token: ${token}`, 'AuthGuard');

    if (token) {
      // 本地jwt方式
      // const user = await this.verifyToken(token, process.env.SECRET);
      // request.user = user;

      // redis单点登陆方式
      // 拿到token反解出里面的用户id,然后用用户id去redis里面查询redis里面的的token是否与当前的一致
      const currentUserId = jwtUtil.decodeToken(token);
      const redisData = await this.redisUtilsService.get(currentUserId);

      console.log(JSON.stringify(redisData), 'redis数据');

      if (Object.is(token, redisData.token)) {
        request.user = redisData.user;
        return true;
      } else {
        throw new HttpException('token已经失效,请重新登录', HttpStatus.UNAUTHORIZED);
      }
    } else {
      throw new HttpException('你还没登录,请先登录', HttpStatus.UNAUTHORIZED);
    }
  }

  /**
   * @param {token}: token
   * @param {secret}: secret
   * @return:
   * @Description: 校验用户传递过来的token
   * @Author: wupeng
   * @LastEditors: wupeng
   * @Date: 2019-07-31 12:56:01
   */
  private verifyToken(token: string, secret: string): Promise<any> {
    return new Promise((resolve, reject) => {
      jwt.verify(token, secret, (error, payload) => {
        if (error) {
          console.log('-----------error start--------------');
          console.log(error);
          console.log('-----------error end--------------');
          reject(error);
        } else {
          resolve(payload);
        }
      });
    });
  }

  /**
   * @Author: wupeng
   * @Date: 2020-01-23 12:01:38
   * @LastEditors: wupeng
   * @Description: 根据key从一段url中获取query值
   * @param urlPath {String} url地址
   * @param key {String} 获取单独的一个key
   * @return {string|object}
   */
  private getUrlQuery(urlPath: string, key?: string): string | { [propsName: string]: any } {
    const query = url.parse(urlPath, true).query
    if (key) {
      return query[key]
    } else {
      return query;
    }
  };
}
