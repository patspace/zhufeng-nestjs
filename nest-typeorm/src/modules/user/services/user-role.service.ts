import { Injectable, Logger, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository, getManager, EntityManager } from "typeorm";
import { UserRoleEntity } from "../entities/user-role.entity";
import { ObjectType } from 'src/types';

@Injectable()
export class UserRoleService {
  constructor(
    @InjectRepository(UserRoleEntity)
    private readonly userRoleRepository: Repository<UserRoleEntity>
  ) {
  }

  async assignRole(data: ObjectType): Promise<any> {
    const { userId, roleList } = data;
    /**
     * 1.先将表中userId的删除
     * 2.重新插入数据
     * 这个地方必须加上事务
     */
    return getManager()
      .transaction(async (entityManager: EntityManager) => {
        await entityManager.delete(UserRoleEntity, { userId: Number(userId) });
        for (const item of JSON.parse(roleList)) {
          await entityManager.save(UserRoleEntity, { userId, roleId: Number(item) });
        }
      }).then(() => {
        return '分配角色成功';
      }).catch((e) => {
        Logger.error('用户角色分配错误', e);
        throw new HttpException('用户分配角色错误', HttpStatus.OK);
      })
  }
}
