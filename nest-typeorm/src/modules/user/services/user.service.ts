import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { UserEntity } from '../entities/user.entity';
import { ObjectType } from '../../../types';
import { Logger } from "@nestjs/common";
import { classToPlain } from 'class-transformer';

@Injectable()
export class UserService {

  private readonly logger = new Logger();

  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>
  ) {
  }

  // 创建数据,传递一个对象类型的数据
  async createUser(data: { [propName: string]: any }) {
    let result;
    try {
      const user = this.userRepository.create(data);
      result = await this.userRepository.save(user);
    } catch (e) {
      throw new HttpException({
        error: e.sqlMessage,
        message: '新建用户出错'
      }, HttpStatus.OK);
    }

    return classToPlain(result, {  });
  }

  // 删除用户
  async deleteUserById(id: number): Promise<UpdateResult> {
    return await this.userRepository.update(id, { isDel: 1 });
  }

  // 更新用户信息
  async modifyUserById(id: number, data: ObjectType): Promise<UpdateResult> {
    return await this.userRepository.update(id, data);
  }

  // 查询全部的数据
  async userList() {
    this.logger.log('消息测试')
    const list = await this.userRepository.find();
    return classToPlain(list);
    // return await this.userRepository.find();
  }

  // 查询单个用户
  async userById(id: number): Promise<UserEntity> {
    return await this.userRepository.findOne(id);
  }
}
