import {Controller, Get, Post, Delete, Body, Param, ParseIntPipe, Patch, UseGuards } from '@nestjs/common';
import { UpdateResult } from 'typeorm';
import { UserService } from '../services/user.service';
import { UserEntity } from '../entities/user.entity';
import { ObjectType } from '../../../types';
import { CreateUserTdo } from '../dto/create.user.dto';
import { AuthGuard } from "../../../guard/auth.guard";

@Controller('user')
@UseGuards(AuthGuard)
export class UserController {
  constructor (
    private readonly userService: UserService,
  ) { }

  @Post()
  async createUser(
    @Body() data: CreateUserTdo
  ) {
    console.log(data)
    return this.userService.createUser(data);
  }

  @Delete(':id')
  async deleteUserById(
    @Param('id', new ParseIntPipe()) id: number
  ): Promise<UpdateResult> {
    return await this.userService.deleteUserById(id);
  }

  @Patch(':id')
  async modifyUserById(
    @Param('id', new ParseIntPipe()) id: number,
    @Body() data: ObjectType
  ): Promise<UpdateResult> {
    return await this.userService.modifyUserById(id, data);
  }

  @Get()
  async userList() {
    return await this.userService.userList();
  }

  @Get(':id')
  async userById(
    @Param('id', new ParseIntPipe()) id: number
  ): Promise<UserEntity> {
    return await this.userService.userById(id);
  }
}
