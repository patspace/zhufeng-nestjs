import { Controller, Post, Body } from '@nestjs/common';
import { ObjectType } from 'src/types';
import { UserRoleService } from "../services/user-role.service";

@Controller('user-role')
export class UserRoleController {
  constructor(
    private readonly userRoleService: UserRoleService
  ) {
  }

  @Post()
  async assignRole(
    @Body() data: ObjectType
  ): Promise<any> {
    return await this.userRoleService.assignRole(data);
  }
}
