import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './entities/user.entity';
import { UserRoleEntity } from './entities/user-role.entity';
import {UserController} from "./constrollers/user.controller";
import {UserService} from "./services/user.service";
import { UserRoleController } from './constrollers/user-role.controller';
import { UserRoleService } from './services/user-role.service';
import { LoginController } from './login/login.controller';
import { LoginService } from './login/login.service';
import { ToolsService } from '../../tools/tools.service';
import { RedisUtilsService } from '../redis-utils/redis-utils.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserEntity,
      UserRoleEntity
    ])
  ],
  controllers: [UserController, UserRoleController, LoginController],
  providers: [UserService, UserRoleService, LoginService, ToolsService, RedisUtilsService]
})
export class UserModule {}
