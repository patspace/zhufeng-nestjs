import { Injectable } from '@nestjs/common';
import { Repository } from "typeorm";
import { LoginDto } from '../dto/login.dto';
import { UserEntity } from "../entities/user.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { ToolsService } from "../../../tools/tools.service";
import { jwt } from '../../../utils/jwt';
import { RedisUtilsService } from "../../redis-utils/redis-utils.service";

@Injectable()
export class LoginService {

  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private readonly toolsService: ToolsService,
    private readonly redisUtilsService: RedisUtilsService
  ) {
  }

  async login(data: LoginDto): Promise<any | string> {
    // 根据用户名去查询数据,然后验证密码
    const { username, password } = data;
    const user = await this.userRepository.findOne({ where: { username } });
    if (user && this.toolsService.checkPassword(password, user.password)) {
      // 通过redis单点登陆方式
      // 1.生成token
      const token = jwt.createToken(String(user.id));
      // 2.token存到到redis中
      const redisData = {
        token,
        user,
      }
      await this.redisUtilsService.set(String(user.id), redisData);

      return user.toResponseObject(true);
    } else {
      return '账号或密码错误';
    }
  }
}
