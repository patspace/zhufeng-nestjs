import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PostsEntity } from "../entities/posts.entity";
import { ObjectType } from 'src/types';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(PostsEntity)
    private readonly postsRepository: Repository<PostsEntity>
  ) {
  }

  async createPosts(data: ObjectType): Promise<PostsEntity> {
    const posts = await this.postsRepository.create(data);
    return await this.postsRepository.save(posts);
  }

  async deletePostsById(id: number): Promise<string> {
    const { raw: { affectedRows } } = await this.postsRepository.delete(id);
    if (affectedRows) {
      return '删除成功';
    } else {
      return '删除失败';
    }
  }

  async modifyPostsById(id: number, data: ObjectType): Promise<string> {
    const { raw: { affectedRows } } = await this.postsRepository.update(id, data);
    if (affectedRows) {
      return '修改成功';
    } else {
      return '修改失败';
    }
  }

  async postsList(): Promise<PostsEntity[]> {
    return await this.postsRepository.find();
  }

  async postsById(id: number): Promise<PostsEntity> {
    return await this.postsRepository.findOne(id);
  }
}
