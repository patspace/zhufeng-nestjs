import { Module } from '@nestjs/common';
import { TypeOrmModule} from "@nestjs/typeorm";
import { PostsController } from './controllers/posts.controller';
import { PostsService } from './services/posts.service';
import { PostsEntity} from "./entities/posts.entity";

@Module({
  imports: [
    TypeOrmModule.forFeature([
      PostsEntity
    ])
  ],
  controllers: [PostsController],
  providers: [PostsService]
})
export class PostsModule {}
