import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity({ name: 'role'})
export class RoleEntity {
  @PrimaryGeneratedColumn({
    type: 'int',
    name: 'id',
    comment: '主键id'
  })
  id: number

  @Column({
    type: 'varchar',
    length: 50,
    nullable: false,
    name: 'title',
    comment: '角色名称'
  })
  title: string

  @Column({
    type: 'varchar',
    length: 100,
    nullable: true,
    name: 'description',
    comment: '角色描述'
  })
  description: string | null

  @Column({
    type: 'tinyint',
    nullable: false,
    default: 0,
    name: 'is_del',
    comment: '是否删除,1表示删除,0表示正常'
  })
  isDel: number

  @CreateDateColumn({
    type: 'timestamp',
    nullable: false,
    name: 'created_at', // mysql数据库规范是使用下划线命名的,不使用驼峰
    comment: '创建时间'
  })
  createdAt: Date

  @UpdateDateColumn({
    type: 'timestamp',
    nullable: false,
    name: 'updated_at',
    comment: '更新时间',
  })
  updateAt: Date
}
