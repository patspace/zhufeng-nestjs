import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult, DeleteResult } from 'typeorm';
import { RoleEntity } from "../entities/role.entity";
import { ObjectType } from '../../../types';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(RoleEntity)
    private roleRepository: Repository<RoleEntity>
  ) {
  }

  async createRole(data: ObjectType): Promise<RoleEntity> {
    const role = await this.roleRepository.create(data);
    return await this.roleRepository.save(role);
  }

  async deleteRoleById(id: number): Promise<DeleteResult> {
    return await this.roleRepository.delete(id);
  }

  async modifyRoleById(id: number, data: ObjectType): Promise<UpdateResult> {
    return await this.roleRepository.update(id, data);
  }

  async roleList(): Promise<RoleEntity[]> {
    return await this.roleRepository.find();
  }

  async roleById(id: number): Promise<RoleEntity> {
    return await this.roleRepository.findOne(id);
  }
}
