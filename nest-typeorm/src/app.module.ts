import { Module, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './modules/user/user.module';
import { ConfigModule, ConfigService } from 'nestjs-config';
import { RoleModule } from './modules/role/role.module';
import { PostsModule } from './modules/posts/posts.module';
import * as path from 'path';
import { LoggerMiddleware } from './middlewares/log.middleware';
import { LoggerService } from './modules/logger/logger.service';
import { AllExceptionsFilter } from './filters/all-exceptions.filter';
// import { TransformInterceptor } from './interceptor/transform.interceptor';
import { RedisUtilsModule } from './modules/redis-utils/redis-utils.module';
import { RedisModule } from 'nestjs-redis';
import { RedisUtilsService } from './modules/redis-utils/redis-utils.service';

@Module({
  imports: [
    // 配置加载配置文件
    ConfigModule.load(path.resolve(__dirname, 'config', '**/!(*.d).{ts,js}'), {
      modifyConfigName: name => name.replace('.config', ''),
    }),
    // mysql的连接
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '12345678',
      database: 'test',
      synchronize: true, // 自动同步数据模型到数据表中
      logging: ["error"], // 启用不同类型的日志记录
      maxQueryExecutionTime: 1000, // 记录所有运行超过1秒的查询
      entities: [
        __dirname + '/**/*.entity{.ts,.js}'
      ],
    }),
    UserModule,
    RoleModule,
    PostsModule,
    RedisModule.register({ // 配置redis的基本信息
      port: 6379,
      host: '127.0.0.1',
      password: '',
      db: 0
    })
  ],
  controllers: [AppController],
  providers: [
    AppService,
    LoggerService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
    // , {
    //   provide: APP_INTERCEPTOR,
    //   useClass: TransformInterceptor,
    // }
    RedisUtilsService
  ],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(LoggerMiddleware)
      .forRoutes({ path: '/user', method: RequestMethod.GET });
  }
}
