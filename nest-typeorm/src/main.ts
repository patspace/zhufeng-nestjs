import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { test1MiddleWares } from './middlewares/test1';
import { checkLoginMiddleWare } from './middlewares/check-login.middleware';
// import { AuthGuard } from './guard/auth.guard';
import { LoggingInterceptor } from './interceptor/logging.interceptor';
import { ValidationPipe } from "@nestjs/common";
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    // logger: false,
  });

  // 配置api文档信息
  const options = new DocumentBuilder()
    .setTitle('nest framework  api文档')
    .setDescription('nest framework  api接口文档')
    .addBearerAuth({ type: 'apiKey', in: 'header', name: 'token' }) // 设置请求头的token字段
    .setVersion('0.0.1')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(`/docs`, app, document);

  app.useGlobalPipes(new ValidationPipe());
  // 使用自定义中间件
  app.use(test1MiddleWares());
  // app.use(checkLoginMiddleWare());
  // 使用自定义守卫
  // app.useGlobalGuards(new AuthGuard());
  // 使用拦截器
  app.useGlobalInterceptors(new LoggingInterceptor());
  await app.listen(3001);
}
bootstrap();
