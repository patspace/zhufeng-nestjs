import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from '@nestjs/common';

@Catch()
export class AllExceptionsFilter<T> implements ExceptionFilter {
  catch(exception: T, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();

    const status =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;

    // 返回消息兼容性处理
    let msg = 'Internal server error';
    let userMsg = '服务器内部错误';
    if (exception instanceof HttpException) {
      // @ts-ignore
      const exceptionRes = exception.getResponse();
      console.log(exceptionRes)
      // @ts-ignore
      msg = typeof(exceptionRes) === 'string' ? exceptionRes : exceptionRes.error ? exceptionRes.error : msg;
      // @ts-ignore
      userMsg = typeof(exceptionRes) === 'string' ? exceptionRes : Array.isArray(exceptionRes.message) ? exceptionRes.message[0] : exceptionRes.message;
    }

    console.log(exception)
    // TODO: 添加logId
    response.status(status).json({
      data: {},
      error: {
        returnCode: 1, // 按照目前公司约定 0-正常 1-错误
        returnMessage: msg,
        returnUserMessage: userMsg
      },
      logId: ''
    });
  }
}
