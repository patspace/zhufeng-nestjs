import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class TransformInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map(({ data, code = 1, msg = 'success', userMsg = '请求成功' }) => {
        return{
          data,
          error: {
            returnCode: code,  // 按照目前公司约定 0-正常 1-错误 可自定义配置错误码
            returnMessage: msg,
            returnUserMessage: userMsg
          },
          logId: '' // TODO: 添加logId
        }
      })
    );
  }
}
