import { Request, Response } from 'express';

export const checkLoginMiddleWare = (/*可以往中间件中传递参数*/) => {
  return (req: Request, res: Response, next: any) => {
    console.log(req.headers)
    let token = req.headers.token
    if(token){
      next();
    }else{
      res.send('没有token')
    }
    next();
  }
}
