import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NestLogModule } from './nest-log/nest-log.module';

@Module({
  imports: [NestLogModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
