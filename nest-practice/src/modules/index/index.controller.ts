import { Controller, Get, Render } from '@nestjs/common';

@Controller('index')
export class IndexController {
  @Get()
  @Render('index')
  getIndex(): any {
    return { name: '哈哈' };
  }
}
