import {Body, Controller, Get, Post, Response, Render, Request} from '@nestjs/common';

@Controller('login')
export class LoginController {
  @Get()
  @Render('login')
  loginPage(
    @Request() req
  ) {
    req.session.name = 'wupeng';
    // 这里的数据到时候讲到数据库，服务层的时候直接从数据库拉取数据,现在先写个假数据
    return { 'title': '登录页面' } // 返回给ejs模板的数据
  }

  @Post()
  login(@Body() body, @Response() res) {
    console.log(body); // 获取表单中提交的数据
    // res.cookie('username', body.username, { maxAge: 1000 * 60 * 60 * 24, httpOnly: true, signed: true });
    res.redirect('/user/1'); // 重定向到用户首页
  }
}
