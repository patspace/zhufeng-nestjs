import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { User } from '../../entity/user.entity';
import { UserExtend } from '../../entity/user-extend.entity';
import { Posts } from '../../entity/post.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, UserExtend, Posts])],
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule {}
