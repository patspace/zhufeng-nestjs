import {Controller, Get, Query, Param, ParseIntPipe, Post, Body, Request} from '@nestjs/common';
import { UserService } from './user.service';
import { LogService } from '../log/log.service';

@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly logService: LogService,
  ) {
  }

  /**
   * @Get([path])当前的path会拼接到@Controller('user')到里面user的路径后面，不写就表示为空的
   */
  @Get()
  async userList(
    // @Query() query: any,
    // @Query('name') name: string,
    // @Param() params: any,
    // @Param('id', new ParseIntPipe()) id: string,
    // @Request() req
  ): Promise<any[]> {
    // console.log(query);
    // console.log(name);
    // console.log(params);
    // console.log(id);
    // console.log(typeof id);
    // console.log(req.signedCookies, '当前的cookie');
    // console.log(req.session);
    // 控制层访问服务层的userList方法
    // this.logService.log('运行了userList控制器');
    // 控制层访问服务层的userList方法
    return await this.userService.userList();
  }

  @Get(':id')
  async user(
    @Param('id', new ParseIntPipe()) id: string
  ) {
    return await this.userService.find(id);
  }

  @Post()
  async addUser(
    @Body() body: any
  ) {
    // 这种写法适合大规模的提交参数,自己又不想一个一个去校验
    return await this.userService.add(body);
  }
}
