import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import "reflect-metadata";
import { Repository } from "typeorm";
// 引入刚刚定义的实体类
import { User } from "../../entity/user.entity";
import { UserExtend } from "../../entity/user-extend.entity";
import { Posts } from "../../entity/post.entity";

@Injectable()
export class UserService {

  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(UserExtend) private userExtendRepository: Repository<UserExtend>,
    @InjectRepository(Posts) private postsRepository: Repository<Posts>,
  ) {
  }

  // 查询所有用户信息
  async userList(): Promise<User[]> {
    return this.usersRepository.find();
  }

  // 查询单个用户信息
  async find(id): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  // 新增用户
  async add(params): Promise<any> {
    const { username, password, mobile, address } = params;
    const user = new User();
    user.username = username;
    user.password = password;
    user.isDel = 0;
    user.createdAt = new Date();
    user.updateAt = new Date();

    const userExtend = new UserExtend();
    userExtend.mobile = mobile;
    userExtend.address = address;

    // 关联两个数据模型
    userExtend.user = user;

    // 帖子一
    // const posts1 = new Posts();
    // posts1.title = '文章一';
    // posts1.content = '文章一内容';

    // 帖子二
    // const posts2 = new Posts();
    // posts2.title = '文章二';
    // posts2.content = '文章二内容';
    //
    // user.posts = [posts1, posts2];

    // 必须先保存用户表,因为他要提供主键出来
    // await this.postsRepository.save(posts1);
    // await this.postsRepository.save(posts2);
    const userInfo = await this.usersRepository.save(user);
    const userExtendInfo = await this.userExtendRepository.save(userExtend);

    // return await this.usersRepository.findOne({
    //   where: {
    //     id: userInfo.id
    //   },
    //   relations: ['userDetail']
    // });

    delete userExtendInfo.user;
    return Promise.resolve({
      ...userInfo,
      ...userExtendInfo
    })
  }
}
