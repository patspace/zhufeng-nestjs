import { NestFactory } from '@nestjs/core';
import 'dotenv/config';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';
// 引入包
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import * as cookieParser from 'cookie-parser';
import * as session from 'express-session';

const PORT = process.env.PORT || 3000;

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  // 配置静态文件的目录
  // 方式一是直接访问:localhost:4000/1.jpg
  // app.useStaticAssets(join(__dirname, '../public'));
  // 方式二是访问:localhost:4000/static/1.jpg但是在public文件夹下不需要创建static目录
  app.useStaticAssets(join(__dirname, '../public'), {
    prefix: '/static/'
  });

  // 配置视图文件的目录
  app.setBaseViewsDir(join(__dirname, '../views'));
  app.setViewEngine('ejs');

  // cookie解析
  // app.use(cookieParser(process.env.SECRET));
  // 配置中间件使用session,加盐是123456(随便写的)
  app.use(session({ secret: process.env.SECRET, cookie: { maxAge: 60000 } }))

  await app.listen(PORT, () => {
    Logger.log(`服务已经启动,请访问:http://wwww.localhost:${PORT}`);
  });
}
bootstrap();
