import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './modules/user/user.module';
import { LogModule } from './modules/log/log.module';
import { IndexModule } from './modules/index/index.module';
import { LoginModule } from './modules/login/login.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from 'nestjs-config';
import * as path from 'path';

@Module({
  imports: [
    // 配置加载配置文件
    ConfigModule.load(path.resolve(__dirname, 'config', '**/!(*.d).{ts,js}'), {
      modifyConfigName: name => name.replace('.config', ''),
    }),
    TypeOrmModule.forRoot(),
    LogModule.register('user'),
    UserModule,
    IndexModule,
    LoginModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [LogModule]
})
export class AppModule {}
